# -*- coding: utf-8 -*-
"""
Base classes for pyagstools.

@author: Tõnis Kärdi
@contact: tonis.kardi@gmail.com
@organization: FIE Ülli Reimets
@copyright: 2014-2015, FIE Ülli Reimets

Classes for handling ESRI featuresets and geometry objects.

Further information and in-depth example usage see the README file.

For licensing information see the LICENSE file.

Documentation is written for epydoc using the Epytext Markup Language.
"""

import json
import json_utils
import requests
import urlparse

#from numpy import array

#{ All-purpose support classes

class _N(object):
    """An all-purpose support class."""
    def __init__(self, meta):
        """Init routine.

        Sets primary properties from the input JSON object.
        @param meta: metadata on this object.
        @type meta: C{dict}
        """
        for a, b in meta.items():
            if isinstance(b, (list, tuple)):
                setattr(self, a, [
                    _N(x) if isinstance(x, dict) else x for x in b])
            elif a == 'geometry':
                setattr(self, a, Geometry(b))
            else:
                setattr(self, a, _N(b) if isinstance(b, dict) else b)
            try:
                getattr(self, a).__name__ = a
            except:
                pass
            
    def _to_json(self):
        """Returns this instance's attributes as a json object.

        Basically a way to return this objects JSON notation.
        
        Inspired by http://stackoverflow.com/a/15538391

        @return: this instance's attributes as a json object.
        @rtype: C{dict}
        """
        return json.loads(
            json.dumps(
                self,
                #no need for privates!
                #default=lambda o: o.__dict__,
                default=lambda o: dict(
                    (k, v) for k, v in o.__dict__.items() \
                        if not k.startswith('_')),
                sort_keys=True)
            )

    def _try_convert(self, val):
        try:
            assert val != None
            val = val.strip()
            if val == '':
                val = None
            assert val != None
            val = self._try_type(val, int)
            if not isinstance(val, int):
                val = self._try_type(val, float)
        except AssertionError as ae:
            pass
        return val

    def _try_type(self, val, _type):
        try:
            assert val != None
            val = _type(val)
        except:
            pass
        return val
    
    @property
    def json(self):
        """This instance as a JSON object."""
        return self._to_json()

    def __repr__(self):
        return "<%s %s at %s>" % (
            self.__dict__.get(
                "type", self.__dict__.get(
                    "__name__", self.__class__.__name__)).replace(' ', ''),
            self.__dict__.get("url", 'object'),
            hex(id(self)))


class Meta(_N):
    """Base class for ArcGISServer URL-openable resources.

    Instance properties will be retrived from the init method input <url>. It
    defaults to C{None}, meaning no instance properties are set up.
    """
    url = None
    _token = None
    def __init__(self, url=None, token=None):
        if token:
            try:
                # check if token can be accessed the way we want.
                assert hasattr(token, 'refresh')
                assert hasattr(token, 'token')
                assert hasattr(token, 'expires')
            except (AssertionError, AttributeError) as ae:
                raise TypeError(
                    'Input argument "token" should be None or a TokenServer')
        if url:
            self.url = '%s/' % url.rstrip('/')
            self._token = token
            meta = self._open(f='json')
        else:
            meta = {}
        super(Meta, self).__init__(meta)
        self._validate()

    def _parse_params(self, **kwargs):
        for k, v in kwargs.iteritems():
            if isinstance(v, dict):
                kwargs[k]=json.dumps(v)
        return kwargs

    def _parse_method(self, method, **kwargs):
        """Finds a suitable method for doing the request.

        Query parametes are passed as keyword argumets.

        @param method: the name of HTTP method to use, e.g C{get}, C{post}.
        @type method: C{str}
        @return: A tuple of a L{requests} callable object and query
            input params.
        @rtype: C{tuple}
        """
        _method = getattr(requests, method)
        kwargs = self._parse_params(**kwargs)
        if self._token != None:
            kwargs['token'] = self._token.refresh()
        if method == 'get':
            params = {'params':kwargs}
        elif method == 'post':
            params = {'data':kwargs}
        return _method, params        

    def _open(self, relative_url=None, method='get', **kwargs):
        """Query an ArcGISServer HTTP/HTTPS resource.

        The URL-to-open is constructed from this instance's base url
        and input <relative_url>, query parameters are given in as
        keyword arguments.

        @param relative_url: The relative url component in respect to
            to this instance's base L{url}
        @type relative_url: C{str}
        @param method: L{requests} method to use, e.g C{get}, C{post} etc.
            (NB! ArcGISServer supports only get and post)
        @type method: C{str}
        @keyword f: the output format for response. Can be one of C{json},
            C{html}, C{image} depending on the queried resource type.
            If this keyword is omitted or any other, response is returned
            as string-content.
        @type f: C{str}
        """
        url = urlparse.urljoin(self.url, relative_url)
        _method, params = self._parse_method(method, **kwargs)
        r = _method(url, **params)
        try:
            r.raise_for_status()
            if kwargs.get('f') == 'json':
                response = r.json()
                assert not response.has_key('error')
            else:
                response = r.content
        except AssertionError:
            raise Exception(
                "Code: %(code)s - %(message)s. Details: %(details)s" %(
                response.get('error', {})))
        except:
            raise
        finally:
            r.close()
        return response

    def _validate(self):
        """Validates instance properties.

        Override this in subclasses if needed.
        
        @raise TypeError: If instance validation fails.
        """
        try:
            assert self.type.replace(' ', '') == self.__class__.__name__
        except (AssertionError, AttributeError) as ae:
            msg = '"%s" does not seem to be a %s.' % (
                    self.url, self.__class__.__name__)
            if self.__dict__.has_key('type'):
                msg += ' Try using %s instead' % self.type.replace(' ', '')
            raise TypeError(msg)
        

#{ Definition of "data types"

class Geometry(_N):
    """Geometry object support class.

    Defines methods and properties for accessing coordinates and
    conversion interfaces, such as the __geo_interface__.
    """
    def __init__(self, geometry):
        """Init routine for the Geometry type.

        During the init routine functions for accessing a geometry's
        properties are "defined" how they should be accessed. Input
        geometry json is not validated. It is up to the caller to
        make sure the geometry json is correct.

        The input json is expected to have either C{rings}, C{paths},
        C{points}, or C{x} and C{y} keys present in accordance with the
        ESRI JSON geometry model. Note! Currently C{envelope}-types
        are not implemented.

        @param geometry: ESRI geometry in JSON notation.
        @type geometry: C{dict}
        @raise TypeError: If geometry type cannot be established from
            input ESRI JSON.
        """
        super(Geometry, self).__init__(geometry)
        if geometry.has_key('rings'):
            self._get_interior = self._get_rings
            self._get_boundary = self._get_rings_boundary
        elif geometry.has_key('paths'):
            self._get_interior = self._get_paths
            self._get_boundary = self._get_paths_boundary
        elif geometry.has_key('points'):
            self._get_interior = self._get_points
            msg = 'A multipoint does not have a boundary'
            self._get_boundary = lambda: self._raise_attribute_error(msg)
        elif geometry.has_key('x') and geometry.has_key('y'):
            self._get_interior = self._get_xy
            msg = 'A point does not have a boundary'
            self._get_boundary = lambda: self._raise_attribute_error(msg)
        # should we, or should we not?
        else:
            raise TypeError("Invalid input geometry definition.")

    def _get_paths(self):
        """[x, y] coordinate sequence for this linestring/multilinestring.

        This is not technically the same "interior" as point-set-theory
        "interior" of a linestring, rather the "defining function".
        For the lack of a better term this will have to do.

        For the sake of the original data itegrity, coordinates are sliced
        from the original and a copy not the original definition is
        returned.

        @return: a coordinate sequence in the form
            C{[[[x11, y11], [x12, y12], ..., [x1n, y1n]], ...,
            [xa1, ya1], [xa2, ya2], ..., [xan, yan]]]} where C{a} denotes
            the number of multilinestring parts and C{n} the the number
            of vertioes in that part.
        @rtype: C{list}
        @raise TypeError: if called on a non-polyline geometry
            type.
        """
        try:
            return self.paths[:]
        except AttributeError as ae:
            raise TypeError("This geometry is not linear!")

    def _get_paths_boundary(self):
        """[x, y] coordinate sequence of this (multi)linestring's boundary

        According to the point-set theory, a linestring's boundary consists
        of it's first and last point, therefore a two-point iterable is
        returned.

        For the sake of the original data itegrity, coordinates are sliced
        from the original and a copy, not the original definition is
        returned.
        
        @return: a coordinate sequence in the form
            C{[[x1, y1], [xn, yn]]} where n denotes the number of vertices
            in this linestring. If this linestring is a multitype, the first
            and last vertex coordinates for each linestring are returned
        @rtype: C{list}
        @raise TypeError: if called on a non-polyline geometry
            type.
        """
        try:
            _g = []
            for path in self.paths:
                _g.extend([path[0][:], path[-1][:]])
            return _g
        except AttributeError as ae:
            raise TypeError("This geometry is not linear!")

    def _get_points(self):
        """[x, y] coordinate sequence for this multipoint.

        For the sake of the original data itegrity, coordinates are sliced
        from the original and a copy, not the original definition is
        returned.
        
        @return: a coordinate sequence in the form
            C{[[x1, y1], [x2, y2], ..., [xn, yn]]} where n denotes the
            number of points in the multipoint geometry.
        @rtype: C{list}
        @raise TypeError: if called on a non-multipoint geometry
            type.
        """
        try:
            return self.points[:]
        except AttributeError as ae:
            raise TypeError("This geometry is not a multipoint!")

    def _get_rings(self):
        """[x, y] coordinate sequence for this polygon/multipolygon.

        This is not technically the same "interior" as point-set-theory
        "interior" of a polygon, but for the lack of a better term
        this will have to do.

        All multipolygons are split up and treated as polygons.
        Polygons with holes (e.g. donuts) are squashed into a single
        list with the exterior ring supposedly in the clockwise order
        and interior ring(s) counterclockwise (for matplotlib plotting
        purposes). Therefore the returned sequence will not be
        topologically correct (the first and last point will not match).
        For a topologically sound coordinate representation please
        have a look at the Geometry.__geo_interface__

        For the sake of the original data itegrity, coordinates are sliced
        from the original and a copy, not the original definition is
        returned.

        @return: a coordinate sequence in the form
            C{[[[x1, y1], [x2, y2], ..., [xn, yn]]]} where n denotes
            the number of vertices in the polygon geometry (including
            exterior and interior rings). In case of multi-polygons the
            return would be in the form
            C{[[poly1, poly2, ..., polyn]]} where n denotes the number
            of participating single-part geometries, each with their own
            coordinate sequence.
        @rtype: C{list}
        @raise TypeError: if called on a non-polygonal geometry
            type.
        """
        try:
            _g = []
            for ring in self.rings:
                if json_utils.is_clockwise(ring):
                    _g.append(ring[:])
                else:
                    _g[-1].extend(ring[:])
        except AttributeError as ae:
            raise TypeError("This geometry is not polygonal!")
        return _g

    def _get_rings_boundary(self):
        """[x, y] coordinate sequence for this (multi)polygon's boundary

        For the sake of the original data itegrity, coordinates are sliced
        from the original and a copy, not the original definition is
        returned.

        @return: a coordinate sequence in the form
            C{[[[x1, y1], [x2, y2], ..., [xn, yn]]]} where n denotes
            the number of vertices in the polygon geometry's ring. Exterior
            and interior rings aswell as multiparts are represented as
            separate sequences. So that a multipolygon consisting of an
            exterior with two interior rings (holes) and an exterior with
            one interior ring becomes C{[[exterior1], [interior11],
            [interior12], [exterior2], [interior21]}.
        @rtype: C{list}
        @raise TypeError: if called on a non-polygonal geometry
            type.
        """
        try:
            return self.rings[:]
        except AttributeError as ae:
            raise TypeError("This geometry is not polygonal!")

    def _get_xy(self):
        """[x, y] coordinate sequence for this point.

        For the sake of the original data itegrity, coordinates are sliced
        from the original and a copy, not the original definition is
        returned.

        @return: a coordinate sequence in the form
            C{[[x, y]]}
        @rtype: C{list}
        @raise TypeError: if called on a non-point geometry (includes
            multipoint) geometry type. 
        """
        try:
            return [[self.x, self.y]]
        except AttributeError as ae:
            raise TypeError("This geometry is not a point!")
    
    def _raise_attribute_error(self, msg):
        """Raises an AttributeError using the specified message.

        @param msg: AttributeError message to convey.
        @raise AttributeError: with the specific message called.
        """
        raise AttributeError(msg)

    @property
    def interior(self):
        """This geometry's interior's coordinate sequence.

        Use this for plotting geometries with matplotlib.
        """
        return self._get_interior()

    @property
    def boundary(self):
        """This geometry's boundary's coordinate sequence.

        Use this for plotting geometries' "edges" with matplotlib.
        """
        return self._get_boundary()

    @property
    def __geo_interface__(self):
        """A geo-interface for this geometry.

        Implemented as defined in https://gist.github.com/sgillies/2217756
        """
        return json_utils.to_geojson_geometry(self.__dict__)

#{ 

class FeatureSet(_N):
    """A class to represent a FeatureSet.

    FeatureSets in ESRI terminology are a lightweight representation of
    a feature class/layer. As there is no concrete structure involved, we
    base this loosely on our all purpose support. So that whatever
    the keys in the key-val store, we'll get it all.
    """
    def __init__(self, featureSet):
        """Init routine.

        @param featureSet: an input ESRI feature set.
        @type featureSet: C{dict}
        """
        super(FeatureSet, self).__init__(featureSet)

    def adapt_geometry(self, adapter):
        """Outputs an iterable of geometries adapted to a particular type.

        @param adapter: a class/function implementing the
            __geo_interface__ such as shapely.geometry.asShape
        @type adapter: C{callable}
        @return: an iterable of adapted geometries from this
            feature set.
        @rtype: C{generator}
        """
        return [adapter(f.geometry) for f in self.features]

    def to_geojson(self):
        """Outputs this instance as a GeoJSON object

        @return: this featureset's representation in GeoJSON.
        @rtype: C{dict}
        """
        return json_utils.to_geojson(self.json)

    def to_json(self):
        """Outputs the original ESRI JSON object for this instance

        @return: this featureset's representation in ESRI JSON.
        @rtype: C{dict}
        """
        return self.json

    @property
    def interior(self):
        """x, y coordinate sequence for all features.

        This is not technically the same "interior" as point-set-theory
        "interior", but for the lack of a better term this will have to do.

        All multi-type objects are split up and treated as their
        singular parts.
        Polygons with holes (donuts) are squashed into a single
        list with the exterior ring supposedly in the clockwise order
        and interior ring(s) counterclockwise.
        Linestrings are points are returned untreated.
        """
        g = []
        for f in self.features:
            g.extend(f.geometry.interior)
        return g

    @property
    def boundary(self):
        """x, y coordinate sequence of features' boundaries.

        Linestrings for polygons and multipolygons, multipoints for
        linestrings and multilinestrings. A point/multipoint does not
        have a boundary and raises a NotImplemented.
        """
        g = []
        for f in self.features:
            g.extend(f.geometry.boundary)
        return g


class SpatialReference(Meta):
    """Helper class for handling SR definitions based on spatialreference.org"""
    def __init__(self, **kwargs):
        self.type = 'SpatialReference'
        super(SpatialReference, self).__init__()
        self.url = 'http://www.spatialreference.org/ref/epsg/'
        self.wkid = kwargs.get('wkid')
        self._cache = {}

    def build_cache(self, key):
        """Build cache for SR definitions by format."""
        try:
            assert key in ['proj4', 'json', 'esriwkt', 'prj',
                           'html', 'prettywkt']
            self._cache[key] = self._open('%i/%s/' %(self.wkid, key), 'get')
        except AssertionError:
            raise KeyError('%s' % key)
        return self._cache[key]
    
    def __getitem__(self, key):
        return self._cache.get(key, self.build_cache(key))
    
    @property
    def proj4(self):
        """A proj4 value for this SR."""
        proj4s = self['proj4']
        defs = map(
            lambda _def: len(_def) == 2 and (
                _def[0], self._try_convert(_def[1])) or (_def[0], True),
            (a.split('=') for a in [
                n.strip() for n in proj4s.split('+') if n != '']))
        return dict((key, val) for key, val in defs)
    
