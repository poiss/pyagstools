# -*- coding: utf-8 -*-
"""
Python API for accessing ESRI ArcGISServer resources.

@author: Tõnis Kärdi
@contact: tonis.kardi@gmail.com
@organization: FIE Ülli Reimets
@copyright: 2014-2015, FIE Ülli Reimets

This module can be used to access the different ESRI's ArcGISServer
service resources that are available:
    - GroupLayer
    - FeatureLayer
    - Table
    - ...

Further information and in-depth example usage see the README file.

For licensing information see the LICENSE file.

Documentation is written for epydoc using the Epytext Markup Language.
"""

from base import Meta, FeatureSet, SpatialReference
from collections import OrderedDict
from json_utils import to_geojson_feature
from servers import TokenServer

import concurrent.futures

_FIELDTYPES = {
    'esriFieldTypeInteger': ['int', '10'],
    'esriFieldTypeSmallInteger': ['int', '5'],
    'esriFieldTypeDouble': ['float', '32.10'],
    'esriFieldTypeSingle': ['float', '32.10'],
    'esriFieldTypeString': ['str', '255'],
    'esriFieldTypeOID': ['int', '10'],
    'esriFieldTypeGlobalID': ['str', '255'],
    'esriFieldTypeDate': ['str', '13']
    }

class GroupLayer(Meta):
    """ArcGISServer GroupLayer class. 

    A GroupLayer is a container for a collection of FeatureLayers/Tables/
    etc. As there is minimal functionality associated with this type
    of layer, it will act as our base class for other types of resources.
    """
    def __init__(self, url, token=None):
        """Init routine, discover GroupLayer metadata.

        @param url: HTTP/HTTPS address of a GroupLayer
        @type url: C{str}
        @param token: A TokenServer instance to be used for token-based
            auth. Defaults to C{None}
        @type token: C{TokenServer}
        """
        super(GroupLayer, self).__init__(url, token)

    def get_parentlayer(self):
        pass

    def get_sublayers(self):
        pass


class Table(GroupLayer):
    """ArcGISServer Table class.

    A Table differs from a FeatureLayer in that it does not have an
    associated geometry-type column. 
    """
    def __init__(self, url, token=None):
        """Init routine, discover Table metadata.

        @param url: HTTP/HTTPS address of a table
        @type url: C{str}
        @param token: A TokenServer instance to be used for token-based
            auth. Defaults to C{None}
        @type token: C{TokenServer}
        """
        super(Table, self).__init__(url, token)
        self._filter = {}

    def _get_oids(self, **kwargs):
        """Fetches objectid values for a query specified in kwargs

        Possible keyword values are specified in L{Table.query}. For a complete
        list of keywords see U{the respective docs<
        http://resources.arcgis.com/en/help/rest/apiref/query.html#params>}
        """
        _kwargs = kwargs.copy()
        _kwargs['f'] = 'json'
        _kwargs['returnIdsOnly'] = True
        return self._open('query', 'post', **_kwargs)['objectIds']

    def filter(self, **kwargs):
        """Executes a query expressed in kwargs and returns result.

        For possible keyword arguments have a look at L{query}.
        @return: An iterator over output rows as GeoJSON features.
        @rtype: C{generator}
        """
        self.set_filter(**kwargs)
        return self.__iter__()

    def query(self, **kwargs):
        """Executes a query expressed as kwargs, returns raw response.

        As a minima either C{where}, C{objectIds} or C{geometry} keyword
        has to be present in query keyword arguments for the
        operation to succeed. For a complete list see
        U{the respective docs<http://resources.arcgis.com/en/help
        /rest/apiref/query.html#params>}
        
        @keyword where: specifies a where clause for the query.
            SQL-flavour depends on the data source type of the layer.
        @type where: C{str}
        @keyword objectIds: specifies a list of row
            identificators to return.
        @type objectIds: C{list}
        @keyword geometry: specifies the "other" geometry in a spatial
            query. If using this, take into account C{geometryType}, C{inSR}
            and C{spatialRel} aswell. If no geometry field present
            will have no impact.
        @keyword outFields: a list of fieldnames to
            return. C{['*']}-wildcard here specifies all fields.
        @type outFields: C{list}
        @keyword returnGeometry: specifies whether geometry should be
            returned. (C{True}|C{False}). If no geometry field present
            will have no impact.
        @type returnGeometry: C{bool}
        @keyword returnCountOnly: specifies whether to return only the
            count of features satisfiying this query (C{True}|C{False}).
        @type returnCountOnly: C{bool}
        @keyword returnIdsOnly: specifies whether to return only row
            identifiers.
        @type returnIdsOnly: C{bool}
        @return: Query operation response
        @rtype: C{dict}
        """
        kwargs['outFields'] = ','.join(kwargs.get('outFields', ['*']))
        return self._open('query', 'post', **kwargs)

    def set_filter(self, **kwargs):
        """Set a filter for output features.

        Sets the filter, but does not execute a query. A possible way
        for chaining filters.

        For possible keyword arguments have a a look at L{query}
        """
        outFields = kwargs.get('outFields', ['*'])
        if self.oid_field.json['name'].lower() not in [
            fieldName.lower() for fieldName in outFields] \
                and '*' not in outFields:
            outFields.insert(0, self.oid_field.json['name'])
            kwargs['outFields'] = outFields
        self._filter = kwargs

    @property
    def features(self):
        """All features as specified by the filter."""
        _filter = self._filter.copy()
        if _filter.get('where') == None:
            _filter['where'] = '1=1'
        if _filter.get('f') == None:
            _filter['f'] = 'json'
        if _filter.get('outFields') == None:
            _filter['outFields'] = '*'
        ids = self._get_oids(**_filter)
        _filter['objectIds'] = ','.join('%i' % oid for oid in ids)
        features = self.query(**_filter)
        numReturned = len(features['features'])
        for feature in features['features']:
            yield feature
        if len(ids) > numReturned:
            # Oh wait, but there's more!
            oid_field = self.oid_field
            _ids = set([feature['attributes'].get(oid_field.name) \
                    for feature in features['features']])
            stillToQuery = list(set(ids).difference(set(_ids)))
            filters = []
            for i in range(0, len(stillToQuery), numReturned):
                __filter = _filter.copy()
                __filter['objectIds'] = ','.join(
                    ['%i' % oid for oid in stillToQuery[i:i+numReturned]])
                filters.append(__filter)
            # flow with some threads...
            with concurrent.futures.ThreadPoolExecutor(
                max_workers=5) as executor:
                f_request = {
                    executor.submit(
                        self.query, **filters[i]): i for i in range(
                            len(filters))}
                for smth in concurrent.futures.as_completed(
                    f_request):
                    try:
                        response = smth.result()
                        for feature in response['features']:
                            yield feature 
                    except Exception as e:
                        # do something more intelligent here
                        print e       

    @property
    def oid_field(self):
        """Resource's ID field object.

        @rtype: C{dict}
        """
        field = None
        for field in getattr(self, 'fields', []):
            if getattr(field, 'type') == 'esriFieldTypeOID':
                break
        return field

    @property
    def schema(self):
        """Resouce's schema description for interop purposes.

        @rtype: C{dict}
        """
        if hasattr(self, 'geometryType'):
            ## NB! how about multi-types here?
            ## agserver will use "multi-" in case of points.
            ## do we need to deduce it from data?
            ## or could we lie and say everything is multi?
            geom = getattr(self, 'geometryType').lstrip('esriGeometry')
            if geom == 'Polyline':
                geom = 'LineString'
        else:
            geom = None
        _fields = self._filter.get('outFields', ['*'])
        if _fields[0] == '*':
            _fields = [field.name for field in self.fields]
        props = OrderedDict(
            [i for i in map(
                lambda f: (
                    (hasattr(f, 'length') \
                     and f.type in _FIELDTYPES.keys()) \
                    and (f.name, '%s:%s' %(
                        _FIELDTYPES[f.type][0], f.length))) \
                or (f.type in _FIELDTYPES.keys() \
                    and (f.name, '%s:%s' %(
                        _FIELDTYPES[f.type][0], _FIELDTYPES[f.type][1]))),
                self.fields) if i and i[0] in _fields])
        return {'geometry':geom, 'properties':props}

    def __len__(self):
        """Number of features using the filter that is set."""
        _filter = self._filter.copy()
        if _filter.get('where') == None:
            _filter['where'] = '1=1'
        _filter['returnCountOnly'] = True
        _filter['f'] = 'json'
        return self.query(**_filter)['count']

    def __getitem__(self, key):
        """Get feature by objectid value."""
        try:
            # what about slicing?
            assert isinstance(key, int) # and key >= 0
            item = self._open('%s' % key, 'get', f='json')
            if not item or not item.has_key('feature'):
                raise IndexError('Feature OID out of range')
        except AssertionError:
            raise TypeError('Feature objectid must be a positive integer.')
        return item

    def __iter__(self):
        """Iterate all features, mind the filter though!

        Outputs GeoJSON features for easy integration with other
        Python geostuff.
        
        @return: An iterator over all resource features/rows using
            the predefined filter.
        @rtype: C{list}
        """
        return (
            to_geojson_feature(
                feature,
                oidField=self.oid_field) for feature in self.features)
           
        
class FeatureLayer(Table):
    """ArcGISServer FeatureLayer class.

    A FeatureLayer is a mappable layer with a geometry-type column and
    associated drawingInfo properties (cartography-bit)
    """
    def __init__(self, url, token=None):
        """Init routine, discover FeatureLayer metadata.

        @param url: HTTP/HTTPS address of a table
        @type url: C{str}
        @param token: A TokenServer instance to be used for token-based
            auth. Defaults to C{None}
        @type token: C{TokenServer}
        """
        super(FeatureLayer, self).__init__(url, token)

    @property
    def crs(self):
        """Proj4 value for this layer's CRS.

        This property is available for interoperability with other
        python geospatial packages.
        
        Honors the outSR parameter which might have been set in a
        filter. If no outSR found from filter then layers default SR WKID
        is used.
        
        @rtype: C{dict}
        """
        wkid = self._filter.get('outSR', self.extent.spatialReference.wkid)
        return SpatialReference(wkid=wkid).proj4

    @property
    def __geo_interface__(self):
        fs = self.query(where='1=0',
                        outFields=self._filter.get('outFields', '*'),
                        f='json')
        fs['features'] = [f for f in self.features]
        return FeatureSet(fs).to_geojson()
