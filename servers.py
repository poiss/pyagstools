# -*- coding: utf-8 -*-
"""
Python API for accessing ESRI ArcGISServer services.

@author: Tõnis Kärdi
@contact: tonis.kardi@gmail.com
@organization: FIE Ülli Reimets
@copyright: 2014, FIE Ülli Reimets

This module can be used to access the myriad of "server-types" that ESRI's
ArcGISServer has to offer:

    - TokenServer: for handling ArcGISServer's token-based auth
        mechanism.
    - MapServer: for handling ArcGISServer's MapServer-type resources

Further information and in-depth example usage see the README file.

For licensing information see the LICENSE file.

Documentation is written for epydoc using the Epytext Markup Language.
"""

from base import _N
from base import Meta
import time


class TokenServer(Meta):
    """Class for handling ArcGISServer token-based auth.

    TokenServer simplifies access to ArcGISServer services with
    token-based authetication mechanism. Once a token has been served,
    it will track its expiration time and fetch a new one if needed.
    """
    def __init__(self, url, username, password, expiration=60,
                 client='requestip', referer=None, ip=None):
        """TokenServer initialization routine.

        @param url: URL of tokenserver.
        @param username: username for acquiring a token.
        @param password: password for username.
        @param expiration: life-span of the token in minutes. Default
            is 60 min.
        @param client: client='requestip' is the default.
            From the ArcGIS U{docs<http://resources.arcgis.com\
            /en/help/rest/apiref/generateadmintoken.html#params>}:\n 
            I{The client IP or HTTP Referer for which the
            token is to be generated.}
                - I{If the value is specified as ip, the ip parameter
                must be specified.}
                - I{If the value is specified as requestip (request IP),
                the token is generated for the IP from where the request
                originated.}
                - I{If the value is specified as referer, the referer
                parameter must be specified.}      
        @param referer: From the ArcGIS U{docs<http://resources.arcgis.com\
            /en/help/rest/apiref/generateadmintoken.html#params>}:\n
            I{The base url of the webapp that will invoke
            the REST API Admin. This parameter must be specified if
            the value of the client parameter is referer.}
        @param ip: From the ArcGIS U{docs<http://resources.arcgis.com\
            /en/help/rest/apiref/generateadmintoken.html#params>}:\n
            I{The ip address of the machine that will invoke
            the REST API Admin. This parameter must be specified if
            the value of the client parameter is ip.}
        """
        super(TokenServer, self).__init__()
        self.url = url
        self.params = {
            'request':'gettoken',
            'username':username,
            'password':password,
            'expiration':expiration,
            'client':client,
            'referer':referer,
            'ip':ip,
            'f':'json'}
        self.tokenjson = {}
        self.refresh()

    def _to_json(self):
        """Returns this instance's tokenjson."""
        return {'token':self.token,'expires':self.expires}

    def fetchtoken(self):
        """Fetches and sets a new token."""
        tokenjson = self._open(None, 'post', **self.params)
        self.settoken(tokenjson)
    
    def refresh(self):
        """Returns a ready-for-use token.

        We will try to minimize our pain by checking whether the token
        may be expiring soon (currently less than 30 seconds left). If so
        get another token. Within this instance's life-cycle this is the
        only method we need to call in order to get a valid token.

        @return: A valid token for accessing an ArcGISServer resource
            such as a layer/service etc.
        @rtype: C{str}
        """
        if self.expires == None or \
           self.token == None or \
           self.expires < (time.mktime(time.localtime()) + 30) * 1000:
            self.fetchtoken()
        return self.token

    def settoken(self, tokenjson):
        """Sets the cached tokenjson from input tokenjson.

        Constructs the best before timestamp itself as the agserver
        machine time and our own time might be out of sync.

        There's still going to be problems though if the returned C{expires}
        is not what we asked for: e.g for the exampleserver used in tests:
        whatever expiration we ask for we always get expires in 60 min.
        And there's no notice about that.

        A simple hack-kinda-workaround for that would be for the caller
        to catch the corresponding exception if token is expired and force
        it's renewal with L{TokenServer.fetchtoken} and then retry the request.
        
        @param tokenjson: a JSON object with 'token' and 'expires'
            keys as returned by ArcGISServer token service.
        @type tokenjson: C{dict}
        """
        tokenjson['expires'] = (
            time.mktime(time.localtime()) + \
            self.params['expiration'] * 60) * 1000
        self.tokenjson = tokenjson
    
    @property
    def expires(self):
        """Token's expiration timestamp."""
        return self.tokenjson.get('expires')

    @property
    def token(self):
        """Token's value."""
        return self.tokenjson.get('token')

    def _validate(self):
        pass

    def __repr__(self):
        return "<TokenServer %s, username:'%s', password:'%s' at %s>" % (
            self.url, self.params['username'], '*****', hex(id(self)))


class MapServer(Meta):
    """ESRI MapServer class.

    A MapServer enables access to map and layer content. A MapService can
    be cached (pre-generated tiles) or dynamic (a raster map
    is genereated in response for every request).

    A MapService is composed of layers, some of which (feature layers
    and tables) can be queried aswell.
    """
    def __init__(self, url, token=None):
        """Init routine, discover service metadata.

        @param url: MapServer url address
        @type url: C{str}
        @param token: A TokenServer instance to be used for token-based
            auth. Defaults to C{None}
        @type token: C{TokenServer}
        """
        super(MapServer, self).__init__(url, token)
        # do some validation
        #self._validate()
        if self.singleFusedMapCache == True:
            # generate an instance's tile() method
            doc = self.tile.__doc__
            def _tile(level, row, column):
                return self._open(
                    'tile/%i/%i/%i' % (level, row, column),
                    'get', f='image')
            self.tile = _tile
            self.tile.__doc__ = doc

    def export(self, **kwargs):
        """Exports a map image from the service.

        Exports a map image from this service as specified in
        the keyword arguments. See U{the docs<
        http://resources.arcgis.com/en/help/rest
        /apiref/index.html?export.html#params>} for a complete list
        of parameters and usage examples.
        
        @keyword bbox: bounding box of the exported image as
            C{[xmin, ymin, xmax, ymax]}.
        @type bbox: C{list}
        @keyword bboxSR: spatial reference wkid of C{bbox}. Defaults
            to this instance's C{extent.spatialReference.wkid}.
        @type bboxSR: C{int}
        @keyword size: image size (width, height) of the exported image.
        @type size: C{tuple}
        @keyword dpi: dpi of the exported image. Defaults to 96.
        @type dpi: C{int}
        @keyword imageSR: the spatial reference wkid of the exported image.
            Defaults to this instance's C{extent.spatialReference.wkid}.
        @type imageSR: C{int}
        @keyword format: the format of the exported image. Defaults to
            C{png}
        @type format: C{string}
        @keyword transparent: specifies whether map background will be set
            as transparent. Possible values include C{True}|C{False}.
            Defaults to C{False}.
        @type transparent: C{bool}
        @keyword layers: determines the layers to be shown on the
            exported map as identified by an single operation of C{show}|
            C{hide}|C{include}|C{exclude} and a list of corresponding
            layerids, e.g. C{{"include":[2,6,8,9]}} to include layers
            with ids 2, 6, 8 and 9 to the configuration shown by default.
            Defaults to C{None} resulting in the default configuration from
            the service to be shown.
        @type layers: C{dict}
        @keyword layerDefs: allows for the filtering of features on
            individual layers in the exported map by specifiying a valid
            SQL filter for a layer, e.g. C{{0:"somecolumn >= 10 and
            someothercolumn = 'somevalue'", 1:"thiscolumn is null"}}.
            SQL-flavour depends on the layer's data source type.
        @type layerDefs: C{dict}
        @return: The exported image content. If return request format C{f}
            was C{json} or return image format C{format} was C{pdf}|C{svg}
            a json with the exported image URL is returned (accessible through
            the C{href} value).
        @raise AssertionError: If C{layers} or C{layerDefs} parameters
            are inconsistent.
        @raise KeyError: If C{bbox} or C{size} parameters are
            inconsistent.
        """
        # a little preparation and internal validation...
        if kwargs.get('format') == None:
            kwargs['format'] = 'png'
        if kwargs['format'].lower() in ['svg', 'pdf']:
            # for whatever reason svg and pdf will not be fetched
            # with f=image
            kwargs['f'] = 'json'
        elif kwargs.get('f') == None:
            kwargs['f'] = 'image'
        assert isinstance(kwargs.get('layers', {}), dict)
        if kwargs.get('layers') != None:
            kwargs['layers'] = '%s:%s' % (
                kwargs['layers'].keys()[0],
                ','.join(
                    '%i' % i for i in \
                    kwargs['layers'][kwargs['layers'].keys()[0]]
                    ))
        assert isinstance(kwargs.get('layerDefs', {}), dict)
        if kwargs.get('layerDefs') != None:
            kwargs['layerDefs'] = "{%s}" %\
                ';'.join(
                    ['%i:"%s"' % (k, v) for k, v in \
                     kwargs['layerDefs'].iteritems()])            
        kwargs['bbox'] = ','.join(['%f' % b for b in kwargs['bbox']])
        kwargs['size'] = ','.join(['%i' % b for b in kwargs['size']])
        return self._open('export', 'get', **kwargs)

    def tile(self, level, row, column):
        """Fetches a tile from this MapServer.

        The MapServer instanace needs to support tiles, i.e
        C{MapServer.singleFusedMapCache == True}.

        @attention: ArcGISServer's tiling scheme differs from the
            OGC (or google) tiling scheme!
        @param level: tile zoom level.
        @type level: C{int}
        @param row: tile row number.
        @type row: C{int}
        @param column: tile column number.
        @type column: C{int}
        @raise AttributeError: If this mapserver does not support
            tiles, i.e. C{singleFusedMapCache} is C{False} in capabilities.
        """
        raise AttributeError(
            '"%s" cannot be accessed using tiles' % self.url)

    def _validate(self):
        try:
            assert 'Map' in self.capabilities.split(',')
            _mapName = getattr(self, 'mapName')
            msg = None
        except AssertionError as ae:
            msg = '"%s" has no "Map" capability!' % self.url
        except AttributeError as ate:
            msg = '"%s" does not seem to be a MapServer.' % self.url
            if hasattr(self, 'type'):
                msg += '  Try using %s instead' % self.type.replace(' ', '')
        if msg:
            raise TypeError(msg)

    def __repr__(self):
        return "<MapServer %s at %s>" % (self.url, hex(id(self)))
