# -*- coding: utf-8 -*-
"""
Imprompt tests for pyagstools.

@author: Tõnis Kärdi
@contact: tonis.kardi@gmail.com
@organization: FIE Ülli Reimets
@copyright: 2014-2015, FIE Ülli Reimets

Requires json and requests, optionally also matplotlib and shapely.
Documentation in README.rst is tested using the doctest module.

The services used for the doctests are hosted by Esri it has to be noted
that Esri reserves the right to change these services at any point in time,
be it either content or the service as a whole may be discarded. The whole
sample code restrictions document is accessible from
U{here<http://resources.arcgis.com/en/sample-use-restrictions/>}

Test data is located at tests_data.py

@todo: rewrite tests with python's unittest module.
"""

import doctest
import json
import json_utils
import requests
import tests_data

from pyagstools.base import FeatureSet, Geometry

try:
    import matplotlib.pyplot as plt
    from matplotlib import collections
    import matplotlib as mpl
    has_mpl = True
except:
    print 'Matplotlib tests will not be run'
    has_mpl = False
    
try:
    from shapely.geometry import asShape
    has_shapely = True
except:
    print 'Adapting to Shapely will not be tested'
    has_shapely = False

def check_clockwise(ring):
    """Testing for ring's clock-wisedness"""
    assert json_utils.is_clockwise(ring) == True

def check_counterclockwise(ring):
    """Testing for ring's unclock-wisedness"""
    assert json_utils.is_clockwise(ring) == False

def check_clockwise_typeerror(ring):
    """Testing for is_clockwise TypeError for input ring type."""
    try:
        json_utils.is_clockwise(ring)
        raise Exception('A TypeError was supposed to be raised!')
    except TypeError as ae:
        pass

def check_clockwise_valueerror(ring):
    """Testing for is_clockwise ValueError for input ring malforms."""
    try:
        json_utils.is_clockwise(ring)
        raise Exception('A ValueError was supposed to be raised!')
    except ValueError as ve:
        pass

def check_signedarea(ring):
    """Testing signed-area calculations."""
    assert (json_utils.signed_area(ring) - 100) < 1e-15
    assert (json_utils.signed_area(ring) - 100) > -1e-15

def check_togeojsongeom(esriGeom):
    """Testing geometry conversion from ESRI JSON to GeoJSON"""
    gj = json_utils.to_geojson_geometry(esriGeom)
    #print json.dumps(gj)
    check_geojson_validity(gj)

def check_togeojsonfc(featureSet):
    """Testing featureset conversion from ESRI JSON to GeoJSON"""
    fc = json_utils.to_geojson(featureSet)
    #print json.dumps(fc)
    check_geojson_validity(fc)

def check_geojson_validity(geojson):
    """Check the validity of the derived GeoJSON.

    Use http://geojsonlint.com/ (by Jason Sanford) site for that
    purpose.
    """
    payload = json.dumps(geojson)
    try:
        r = requests.post(tests_data.geojson_validate_url, data=payload)
        r.raise_for_status()
        response = r.json()
        assert response['status'] == 'ok' 
    except AssertionError as ae:
        raise Exception(response['message'])

def check_parse_fs(fSet):
    """Checks a featureset JSON to FeatureSet conversion"""
    fs = FeatureSet(fSet)
    #print json.dumps(fs.to_geojson())
    check_geojson_validity(fs.to_geojson())

def check_fs_adapter(fSet):
    """Checks fs adapting to other types using the __geo_interface__"""
    fs = FeatureSet(fSet)
    for n in fs.adapt_geometry(asShape):
        assert n.is_valid == True

def check_mpl_adapter_p(fSet):
    """Check plotting with matplotlib - polygons/multipolygons."""
    fs = FeatureSet(fSet)
    coll = collections.PolyCollection(
        fs.interior, linewidths=0)
    bords = collections.LineCollection(
        fs.boundary, linewidths=2)
    coll.set_alpha(0.5)
    fig, ax = plt.subplots()
    pc = ax.add_collection(coll)
    pb = ax.add_collection(bords)
    ax.autoscale_view()
    plt.show()

def check_mpl_adapter_ls(fSet):
    """Check plotting with matplotlib - linestrings/multilinestrings."""
    fs = FeatureSet(fSet)
    lins = collections.LineCollection(
        fs.interior, linewidths=2)
    fig, ax = plt.subplots()
    pc = ax.add_collection(lins)
    # plus add first and last point from fs.boundary aswell
    coords = json_utils.array(fs.boundary)
    pts = plt.plot(
        coords[0::, 0], coords[0::, 1], linewidth=0, marker='o')
    ax.autoscale_view()
    plt.show()

def check_mpl_adapter_pt(fSet):
    """Check plotting with matplotlib - points."""
    fs = FeatureSet(fSet)
    coords = json_utils.array(fs.interior)
    fig, ax = plt.subplots()
    pts = plt.plot(
        coords[0::, 0], coords[0::, 1], linewidth=0, marker='o')
    ax.autoscale_view()
    plt.show()

def check_mpl_adapter_pts(fSet):
    """Check plotting with matplotlib - multipoints."""
    pass    
    
def check_geometry_class_pt(geom):
    """Checks the Geometry class for consistency using (multi)point data."""
    g = Geometry(geom)
    check_geojson_validity(g.__geo_interface__)
    interior = g.interior
    assert interior != None
    assert isinstance(interior, list)
    if not 'points' in geom.keys():
        assert len(interior) == 1
    else:
        assert len(interior) > 1
    try:
        # neither a point nor a multipoint has a boundary, check
        # if AttributeError is raised.
        g.boundary
        raise Exception('An AttributeError was supposed to be raised!')
    except AttributeError as ae:
        pass

def check_geometry_class_ls(geom):
    """Checks the Geometry class for consistency using (multi)linestring data.
    """
    g = Geometry(geom)
    check_geojson_validity(g.__geo_interface__)
    interior = g.interior
    assert interior != None
    assert isinstance(interior, list)
    assert geom['paths'] == interior
    boundary = g.boundary
    if len(geom['paths']) == 1:
        # a linestring's boundary is a multipoint of 2
        assert len(boundary) == 2
    else:
        # a multilinestrings's boundary is a multipoint of n with
        # an even number of points.
        assert len(boundary) > 2
        assert len(boundary) % 2 == 0
    
def check_geometry_class_p(geom):
    """Checks the Geometry class for consistency using (multi)polygon data."""
    g = Geometry(geom)
    check_geojson_validity(g.__geo_interface__)
    interior = g.interior
    assert interior != None
    assert isinstance(interior, list)
    if g.__geo_interface__['type'] == 'Polygon':
        # if it is a single-part, only one ring defining
        # the interior
        assert len(interior) == 1
    else:
        # if it's multi ...
        assert len(interior) > 1
    boundary = g.boundary
    assert boundary != None
    assert isinstance(boundary, list)
    assert geom['rings'] == boundary

def check_readme():
    """Run doctests on the README file."""
    doctest.testfile("README.rst")

def run():
    """Run all tests."""
    check_signedarea(tests_data.testring)
    check_clockwise(tests_data.testring)
    tests_data.testring.reverse()
    check_counterclockwise(tests_data.testring)
    check_clockwise_typeerror(
        {'coordinates':[tests_data.testring]})
    check_clockwise_valueerror(
        [[tests_data.testring[i][0]] \
             for i in range(len(tests_data.testring))])
    check_togeojsongeom(tests_data.esri_pt['geometry'])
    check_togeojsongeom(tests_data.esri_mpt['geometry'])
    check_togeojsongeom(tests_data.esri_ls['geometry'])
    check_togeojsongeom(tests_data.esri_mls['geometry'])
    check_togeojsongeom(tests_data.esri_p['geometry'])
    check_togeojsongeom(tests_data.esri_p_holes['geometry'])
    check_togeojsongeom(tests_data.esri_mp_holes['geometry'])
    check_togeojsonfc(tests_data.esri_featureset_p)
    check_parse_fs(tests_data.esri_featureset_p)
    check_parse_fs(tests_data.esri_featureset_ls)
    if has_shapely:
        check_fs_adapter(tests_data.esri_featureset_p)
        check_fs_adapter(tests_data.esri_featureset_ls)
        check_fs_adapter(tests_data.esri_featureset_pt)
        check_fs_adapter(tests_data.esri_featureset_pts)
    if has_mpl:
        check_mpl_adapter_p(tests_data.esri_featureset_p)
        check_mpl_adapter_ls(tests_data.esri_featureset_ls)
        check_mpl_adapter_pt(tests_data.esri_featureset_pt)
        check_mpl_adapter_pt(tests_data.esri_featureset_pts)
    check_geometry_class_pt(tests_data.esri_pt['geometry'])
    check_geometry_class_pt(tests_data.esri_mpt['geometry'])
    check_geometry_class_ls(tests_data.esri_ls['geometry'])
    check_geometry_class_ls(tests_data.esri_mls['geometry'])
    check_geometry_class_p(tests_data.esri_p['geometry'])
    check_geometry_class_p(tests_data.esri_p_holes['geometry'])
    check_geometry_class_p(tests_data.esri_mp_holes['geometry'])
    check_readme()
    
if __name__ == '__main__':
    run()
