# -*- coding: utf-8 -*-
"""
@author: Tõnis Kärdi
@contact: tonis.kardi@gmail.com
@organization: FIE Ülli Reimets
@copyright: 2014, FIE Ülli Reimets
@version: 0.2

Tools for handling ESRI ArcGISServer services and resources in a rather
simplstic way in Python

Further information and in-depth example usage see the README file.

For licensing information see the LICENSE file.

Documentation is written for epydoc using the Epytext Markup Language.
"""

SOMECONSTANT = 1

def open(blaa):
    print blaa
