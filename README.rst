﻿:Author: Tõnis Kärdi
:Contact: tonis.kardi@gmail.com
:Organization: FIE Ülli Reimets
:Copyright: \(c\) 2014, FIE Ülli Reimets
:Website: https://bitbucket.org/poiss/pyagstools
:Issues: https://bitbucket.org/poiss/pyagstools/issues
:Version: 0.2
:License: BSD, see the LICENSE file


Introduction
============
pyagstools hosts some tools needed for processing ESRI ArcGISServer
services and data. This package contains the following modules:
	
- json_utils is a set of utilities for converting geometries/
  features/featuresets in `ESRI JSON`_ format to GeoJSON_ format.
  (added in ver 0.1)
- base is a set of base classes for handling ESRI featuresets
  and geometries (added in ver 0.1) in a reasonably simplistic 
  fashion. Ver 0.2 adds a Meta base class for URL-accessible
  ArcGISServer resources (layers/tables, services and servers).
- servers contains a higher level API for accessing ArcGISServer's
  TokenServer (added in ver 0.2)
- resources enables easy access to ArcGISServer's resources,
  e.g Feature Layers, Tables, etc (added in ver 0.2)

pyagstools depends on the following non-standard python packages:

- numpy_
- requests_
- futures_
	
Epydoc_ is used throughout for doc. Standalone documentation can be created 
from Epytext Markup using::
	
	python epydoc --config=docs/doc.conf -v
	
pyagstools has been written on Python 2.7.
	
Installation
============
Download a zip
--------------
Copy of this package's latest stable as a zipfile can be 
downloaded from 
	https://bitbucket.org/poiss/pyagstools/get/master.zip 
Unzip it to suitable location.

From the source
---------------
	
	git clone https://bitbucket.org/poiss/pyagstools.git
		
Tests
=====
Tests are written as python doctests and can be executed by::

	python tests.py
		
Tests require the doctest module, and optionally matplotlib_ 
(for testing plotting) and shapely_ (for testing conversions).
	
	
Usage
=====
json-utils.py
-------------

The json-utils package contains a set of utilities for converting
geometries/features/featuresets in ESRI JSON format to the GeoJSON format
so they can easily be used in modern GIS applications.

ESRI ArcGISServer FeatureSet to a GeoJSON FeatureCollection
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

A simplistic usage of GETing some data, converting it and saving 
to a local file. The ArcGISServer used here is a version 10 ESRI
sample server

>>> import json_utils
>>> import requests
>>> import json
>>> url = 'http://sampleserver3.arcgisonline.com/ArcGIS/rest/services/Hydrography/Watershed173811/MapServer/0/query'
>>> params = {
...		'where':'objectid<=100',
...		'outFields':'*',
...		'outSR':4326,
...		'f':'json'}
>>> r = requests.get(url, params=params)
>>> r.raise_for_status()
>>> response = r.json()

response is a ESRI JSON FeatureSet (a plain view of a feature class/layer).
Now simply convert it:

>>> fc = json_utils.to_geojson(response)
>>> with open('tests/samples/example.json', 'wb') as f:
...		f.write(json.dumps(fc))
... 
>>> 

And now fire up Your favourite web/desktop GIS to marvel at the
result.

Adapting a FeatureSet to other types
++++++++++++++++++++++++++++++++++++

A Featureset can easily be adapted to other Python types
that make use of the __geo_interface__. For example, consider 
this example using Shapely_:

>>> from pyagstools.base import FeatureSet
>>> from pyagstools import tests_data
>>> fs = FeatureSet(tests_data.esri_featureset_p)
>>> from shapely.geometry import asShape
>>> for n in fs.adapt_geometry(asShape):
...		print n.is_valid, n.buffer(0.001).area
...
True 19.4166099626
True 22.3726201335
True 13.534453622
>>> 

Plotting a FeatureSet using matplotlib_
++++++++++++++++++++++++++++++++++++++

A FeatureSet can be plotted using matplotlib_ or similar

>>> from matplotlib import collections
>>> import matplotlib.pyplot as plt
>>> from numpy import array

Let's add the polygon feature set from tests_data. The 
FeatureSet.interior property returns polygons' (with 
or without holes in them) and multipolygon's coordinates
suitable for plotting with matplotlib.collections. 
As for points, collections can be used, but the 
regular plot command suits aswell.

Please note that FeatureSet.interior is technically not the 
same as point-set theory meaning of interior. Here it is 
simply a sequence of all object coordinates squashed
into a single array with polygon's holes and all.

FeatureSet.boundary returns an iterable of coordinates 
suitable for plotting polygon edges or linestring's 
first and last points. This is especially useful for 
plotting the edges of polygons.

Points and multipoints do not have a boundaries.

But enough talk, let's plot those polygons:

>>> fs = FeatureSet(tests_data.esri_featureset_p)
>>> p_interior = collections.PolyCollection(
... 	fs.interior, linewidths=0)
>>> p_interior.set_alpha(0.5)
>>> p_boundary = collections.LineCollection(
... 	fs.boundary, linewidths=2)

Add a polyline featureset aswell, adding first
and last points (boundaries) of the linestrings.

>>> fs2 = FeatureSet(tests_data.esri_featureset_ls)
>>> l_interior = collections.LineCollection(
... 	fs2.interior, linewidths=2)
>>> l_coords = array(fs2.boundary)

And a point featureset.

>>> fs3 = FeatureSet(tests_data.esri_featureset_pts)
>>> coords = array(fs3.interior)

And now for some plotting...

>>> fig, ax = plt.subplots()
>>> pc = ax.add_collection(p_interior)
>>> pb = ax.add_collection(p_boundary)
>>> lc = ax.add_collection(l_interior)
>>> l_pts = ax.plot(
...		l_coords[0::, 0], l_coords[0::, 1], linewidth=0,
... 	marker='o')
>>> pts = ax.plot(
... 	coords[0::, 0], coords[0::, 1], linewidth=0,
... 	marker='o')
>>> ax.autoscale_view()
>>> #plt.show()
>>> plt.savefig('tests/samples/example.png')
>>> plt.close()
>>> 
	
servers.py
----------
The servers module contains a higher-level API for using ArcGISServer and
it's services. 

Using the MapServer
+++++++++++++++++++

A MapServer enables access to map and layer content. A MapService can
be cached (pre-generated tiles) or dynamic (a raster map
is genereated in response for every request).

A MapService is composed of layers, some of which (feature layers
and tables) can be queried aswell.

Example of using one of ESRI's sampleservers:

>>> from servers import MapServer, TokenServer
>>> msurl = 'http://sampleserver1.arcgisonline.com/ArcGIS/rest/services/PublicSafety/PublicSafetyBasemap/MapServer'
>>> ms = MapServer(msurl)
>>> # browse some service properties:
>>> print ms.documentInfo.Title
Louisville Basemap

Is this service tiled?

>>> assert ms.singleFusedMapCache == True
>>> ms.tileInfo.format
u'JPEG'
>>> t = ms.tile(0,2,4)
>>> # Let's save this to a file
>>> with open('tests/samples/test_tile.jpg', 'wb') as f:
...     f.write(t)
...

And we're done. How about a dynamic map? Output in png format
400*400px with the mapservice default spatialreference and
initial extent - here 'ya go:

>>> bbox = [ms.initialExtent.xmin, ms.initialExtent.ymin, 
... 	ms.initialExtent.xmax, ms.initialExtent.ymax]
>>> m = ms.export(bbox=bbox, format='png', transparent=True,
...     f='image', size=(400,400))
>>> with open('tests/samples/test_dyna.png', 'wb') as f:
...     f.write(m)
...
>>>

The same but with EPSG:2205 spatial reference. NOTE! input bbox
spatial reference can be changed aswell - just feed the appropriate
[xmin, ymin, xmax, ymax] to bbox and specify the bboxSR keyword with
the wkid of Your choice:

>>> m = ms.export(bbox=bbox, format='png', transparent=True,
...     f='image', size=(400,400), imageSR=2205)
>>> with open('tests/samples/test_dyna_2205.png', 'wb') as f:
...     f.write(m)
...
>>>

Getting around the ESRI ArcGISServer token-based auth
+++++++++++++++++++++++++++++++++++++++++++++++++++++
ArcGISServer token-based auth is handled by using the TokenServer class.

>>> # get a token for 60 minutes, discarding the default client=requestip
>>> import time
>>> ts = TokenServer(
...     'https://serverapps10.esri.com/ArcGIS/tokens/', 
... 	'user1',
...     'pass.word1', 60, None)
>>> # .. and that's it. Now we can have a look at the token:
>>> assert len(ts.token) > 0
>>> t = ts.token
>>> # Server and local times have been matched so a token's
>>> # best-before can be traced.
>>> time.localtime() == time.localtime((float(ts.expires)/1000) - 60 * 60)
True

If we need to use a token in a web-request we simply need to call
refresh() prior to the request where a token is needed:

>>> # Simulate that 59 minutes has passed (we subtract this from the expires
>>> # bit). The call to ts.refresh() has to return the same token
>>> # aqcuired before.
>>> ts.tokenjson['expires'] = ts.expires - (59 * 60 * 1000)
>>> ts.refresh() == t
True
>>> # another 31 seconds passes. Now a call to ts.refresh() fetches
>>> # a new token
>>> ts.tokenjson['expires'] = ts.expires - (31 * 1000)
>>> ts.refresh() != t
True

To incorporate a TokenServer instance to a MapServer request simply
pass it on as a second argument
	
>>> msurl = 'https://serverapps10.esri.com/ArcGIS/rest/services/MontgomeryEdit/MapServer'
>>> # This fails:
>>> try:
...     ms = MapServer(msurl)
... except Exception as e:
...     print e.message
...
Code: 499 - Unauthorized access. Details: []
>>>

We'll use the previously initialized TokenServer and export a map image:

>>> ms = MapServer(msurl, ts)
>>> bbox = [ms.initialExtent.xmin, ms.initialExtent.ymin, 
... 	ms.initialExtent.xmax, ms.initialExtent.ymax]
>>> m = ms.export(bbox=bbox, format='png', transparent=True,
...     f='image', size=(400,400))
>>> with open('tests/samples/test_token.png', 'wb') as f:
...     f.write(m)
...
>>> 

resources.py
------------
The resources enables pythonic access to ArcGISServer's resources (tables, 
featurelayers) 

Querying a FeatureLayer
+++++++++++++++++++++++
Input query parameters are passed in as keyword arguments to the 
instance's query method. A full listing of possible kwargs is available
at the `agserver docs site`_.

Let's use the European Environmental Agency's CORINE landcover data 
service and get all artificial surfaces covering approximately the
area of Estonia.

>>> from resources import FeatureLayer
>>> import fiona
>>> flURL = 'http://discomap.eea.europa.eu/arcgis/rest/services/\
... Land/CLC2006_Dyna_LAEA/MapServer/8'
>>> bbox = {
... 	'xmin':21.6748,
... 	'ymin':57.5088,
... 	'xmax':28.3733,
... 	'ymax':59.7712}
>>> fLayer = FeatureLayer(flURL)
>>> queryResult = fLayer.query(
... 	geometry=bbox, 
... 	geometryType='esriGeometryEnvelope',
... 	inSR=4326,
... 	outSR=3301,
... 	outFields=['objectid', 'ID', 'code_06', 'remark'], 
... 	f='json')
>>> assert isinstance(queryResult, dict)
>>> keys = queryResult.keys()
>>> keys.sort()
>>> for key in keys:
...		print key
...	
displayFieldName
exceededTransferLimit
features
fieldAliases
fields
geometryType
spatialReference
>>>

The query method will return a non-hindered ArcGISServer response
(hence it has the f parameter aswell - if left unspecified defaults to 'json')
with as many features (table rows) as the ArcGISServer has been configured to 
return (defaults to 1000 rows). There is no REST-like paging system for ArcGIS 
REST API so one would have to keep track of what has already been queried and 
what not if in need of more/all features than the default transfer limit.

In order to access all the features (possibly satisfying a filter condition), 
the filter method of a FeatureLayer instance can be used. Also note that a 
FeatureLayer instance is iterable, it has a length (the amount of features 
that satisfy conditions set in filter) and individual features can be accessed 
by their OID values. NB! this is not to be confused with the 0-based index 
notation, i.e. aFeatureLayer[-1] will fail if there is no feature with an OID 
value of -1.

Executing the instance's query(**kwargs) method will not make use of the filter 
conditions. On the other hand if a filter is set by the instance's 
filter(**kwargs) which yields a generator or set_filter(**kwargs) which just
sets a filter and returns nothing, the FeatureLayer instance will only be 
iterable over those features that satsify the filter.

The current implementation of filtering requires the presence of the OID field
of the layer. So if it is not specified in the outFields keyword argument, it
will be added automatically.

One other thing to keep in mind is that the generator returned by an 
instance's filter(**kwargs) method as well as iterating the instance itself 
will yield GeoJSON objects. At the same time an instances property called 
features will return a generator with the original ESRI JSON feature notation.

>>> assert len(queryResult['features']) < len(fLayer)
>>> filter = fLayer.filter(
... 	geometry=bbox, 
... 	geometryType='esriGeometryEnvelope',
... 	inSR=4326,
... 	outSR=3301,
... 	outFields=['ID', 'code_06', 'REMARK'])
>>> geoJSONFeatures = list(filter)
>>> assert len(geoJSONFeatures) == len(fLayer)
>>> print len(fLayer)
1309
>>> fLayer[-1] # doctest: +ELLIPSIS
Traceback (most recent call last):
[...]
IndexError: Feature OID out of range
>>> print geoJSONFeatures[0].keys()
['geometry', 'type', 'properties']
>>> esriJSON = fLayer.features.next()
>>> print esriJSON.keys()
[u'geometry', u'attributes']
>>> assert fLayer.oid_field.name in geoJSONFeatures[0]['properties'].keys()
>>>

Using FeatureLayer to save data locally
+++++++++++++++++++++++++++++++++++++++
FeatureLayer and Table both support reading data in the fiona_ style in order
to make it fairly trivial to save data locally in a multitude of geospatial
data formats. In order to achieve this, a FeatureLayer/Table instance's 
schema can be accessed using the schema property (returns an OrderedDict
of fieldnames and fieldtypes). FeatureLayer has also the crs property (returns
a proj4 string) - all the things needed for fiona_.

>>> for key in fLayer.schema:
... 	assert key in ['geometry', 'properties']
>>> 
>>> fLayer.schema # doctest: +ELLIPSIS
{'geometry': u'Polygon', 'properties': OrderedDict([(u'OBJECTID', 'int:10'), (u'code_06', 'str:3'), (u'ID', 'str:18'), (u'REMARK', 'str:20')])}
>>> crs_keys = ['proj', 'ellps', 'x_0', 'y_0', 'lon_0', 'lat_0', 'lat_1', 'lat_2', 
... 	'towgs84', 'units', 'no_defs']
>>> for key in fLayer.crs:
... 	assert key in crs_keys
...
>>> crs_keys.sort()
>>> for key in crs_keys:
... 	print key, fLayer.crs[key]
...
ellps GRS80
lat_0 57.5175539306
lat_1 59.3333333333
lat_2 58
lon_0 24
no_defs True
proj lcc
towgs84 0,0,0,0,0,0,0
units m
x_0 500000
y_0 6375000
>>> 

So in order to save this FeatureLayer instance's data, we can simply:

>>> with fiona.collection('tests/corine_artificial.tab', 'w', 
... 	crs=fLayer.crs,
... 	driver='MapInfo File',
... 	schema=fLayer.schema) as _f:
... 		for feature in fLayer:
... 			_f.write(feature)
... 
>>> 

Bugs or Enhancements?
======================
Found something that's rotten or need a special feature? You can file
an issue at https://bitbucket.org/poiss/pyagstools/issues
	
Acknowledgements
================

- Jason Sanford's geojsonlint (http://geojsonlint.com) is used for validating
  output GeoJSON objects in tests.
- pylayers's (https://github.com/pylayers/pylayers) util.geomutil module was 
  of help for getting a polygon's signed area calculation right (in the numpy
  fashion).
- fiona_ has helped define some things to keep in mind regarding geospatial
  interoperability.
- http://spatialreference.org is the source of all things SRS. pyagstools relies
  heavily on it's data on SRS WKIDs and their definitions.

.. _GeoJSON: http://geojson.org/geojson-spec.html
.. _ESRI JSON: http://resources.arcgis.com/en/help/rest/apiref/index.html?feature.html
.. _numpy: https://pypi.python.org/pypi/numpy
.. _requests: https://pypi.python.org/pypi/requests
.. _epydoc: https://pypi.python.org/pypi/epydoc
.. _matplotlib: https://pypi.python.org/pypi/matplotlib
.. _shapely: https://pypi.python.org/pypi/Shapely
.. _futures: https://pypi.python.org/pypi/futures
.. _fiona: https://pypi.python.org/pypi/fiona
.. _agserver docs site: http://resources.arcgis.com/en/help/rest/apiref/query.html#params