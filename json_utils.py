# -*- coding: utf-8 -*-
"""
Utilities for handling ESRI JSON in Python.

@author: Tõnis Kärdi
@contact: tonis.kardi@gmail.com
@organization: FIE Ülli Reimets
@copyright: 2014-2015, FIE Ülli Reimets

Simple utilities for converting ESRI JSON (as returned
by e.g an ArcGISServer) representations of geometries/
features/featuresets to GeoJSON format.

Further information and in-depth example usage see the README file.

For licensing information see the LICENSE file.

Documentation is written for epydoc using the Epytext Markup Language.
"""

import json
from numpy import array, hstack

#{ General utilities

def is_clockwise(ring):
    """Checks whether input ring cooridinates are in clockwise order.

    @param ring: list of ring coordinates represented as
        [[x1, y1], [x2, y2], ... [xn, yn]]. x-axis is horizontal (East-West)
        y-axis vertical (Nort-South).
    @type ring: C{list}
    @return: False/True for this ring's direction.
    @rtype: C{bool}
    @raise TypeError: input ring coordinates has to be a list
    @raise ValueError: ring coordinate sequence is somehow malformed. 
    """
    try:
        assert isinstance(ring, list)
        a = signed_area(ring)
    except AssertionError as ae:
        raise TypeError("Input ring coordinates should be a list")
    except IndexError as ie:
        raise ValueError("Malformed ring structure")
    return a >= 0

def signed_area(ring):
    """Calculates the signed area of the input ring coordinates.

    Inspired by
    U{http://pylayers.github.io/pylayers/_modules/pylayers/util/geomutil.html}

    @param ring: list of ring coordinates represented as
        [[x1, y1], [x2, y2], ... [xn, yn]]. x-axis is horizontal (East-West)
        y-axis vertical (Nort-South).
    @type ring: C{list}
    @return: signed area of a polygon formed by input coordinate sequence.
    @rtype: C{float} or C{int}
    """
    _ring = array(ring)
    x, y = _ring[0::, 0], _ring[0::, 1]
    p = array([x, y])
    return sum(
        hstack((p[1, 1::], p[1, 0:1])) * (
            hstack((p[0, 2::], p[0, 0:2])) - p[0, :])) / 2

#{ Public converters

def to_geojson(o, includeSrid=True, oidField=None, **kwargs):
    """Shorthand for converting ESRI JSON to GeoJSON.

    Accepts featuresets, single features and single geometries. This
    is the function to call for conversion.
    
    @param o: a featureset/feature in esri JSON notation.
    @type o: C{dict}
    @keyword includeSrid: indicates whether the crs object will
        be included with the featureset. Applicable only for featuresets,
        not features. Defaults to C{False}.
    @type includeSrid: C{bool}
    @keyword oidField: feature id fieldname. Applicable only for single
        features. Defaults to C{None}.
    @type oidField: C{str}
    @return: GeoJSON representation of this featureset/feature.
    @rtype: C{dict}    
    """
    r = to_geojson_geometry
    kwargs['includeSrid'] = kwargs.get('includeSrid', includeSrid)
    kwargs['oidField'] = kwargs.get('oidField', oidField)
    if o.has_key("features"):
        r = to_geojson_featurecollection
    elif o.has_key("geometry"):
        r = to_geojson_feature
    return r(o, **kwargs)

def to_geojson_geometry(esriGeom, **kwargs):
    """Converts geometry in ESRI JSON notation to GeoJSON

    @param esriGeom: geometry object in esri JSON notation.
    @type esriGeom: C{dict}
    @return: GeoJSON representation of this geometry.
    @rtype: C{dict}
    """
    if esriGeom == None:
        gjGeom = None
    elif esriGeom.has_key('paths'):
        gjGeom = _from_paths(esriGeom['paths'])
    elif esriGeom.has_key('rings'):
        gjGeom = _from_rings(esriGeom['rings'])
    else:
        gjGeom = _from_points(esriGeom)      
    return gjGeom

def to_geojson_feature(esriFeat, **kwargs):
    """Converts feature in ESRI JSON notation to GeoJSON

    @param esriFeat: feature object in esri JSON notation.
    @type esriFeat: C{dict}
    @keyword oidField: feature id fieldname. Defaults to C{None}.
    @type oidField: C{str}
    @return: GeoJSON representation of this feature.
    @rtype: C{dict}
    """
    props = esriFeat.get("attributes")
    o = {"type":"Feature",
         "properties": props if props != {} else None,
         "geometry":to_geojson_geometry(esriFeat.get("geometry"))}
    oidField = kwargs.get('oidField')
    if oidField != None and \
        props.get(oidField) != None:
        o["id"] = props[oidField]
    return o

def to_geojson_featurecollection(esriFeatSet, **kwargs):
    """Converts featureset in ESRI JSON notation to GeoJSON

    @param esriFeatSet: featureset object in esri JSON notation.
    @type esriFeatSet: C{dict}
    @keyword includeSrid: Indicates whether the crs object will
        be included. Defaults to C{False}.
    @type includeSrid: C{False}
    @return: GeoJSON representation of this featureset.
    @rtype: C{dict}
    """
    oidField = None
    for field in esriFeatSet.get("fields", []):
        if field.get("type") == "esriFieldTypeOID":
            oidField = field["name"]
            break
    o = {"type":"FeatureCollection",
         "features": [to_geojson_feature(f, oidField=oidField) for \
                      f in esriFeatSet.get("features", [])]}
    if esriFeatSet.has_key("spatialReference") and \
        kwargs.get('includeSrid', False) == True:
        o["crs"] = _from_esri_wkid(esriFeatSet["spatialReference"])
    return o

#{ Private parsers

def _from_paths(paths):
    """Private parser for converting ESRI JSON paths to (Multi)LineString.

    @param paths: path coordinate sequence.
    @type paths: C{list}
    @return: corresponding (Multi)LineString object in GeoJSON.
    @rtype: C{dict}
    """
    _coordinates = paths
    _type = 'MultiLineString'
    if len(_coordinates) == 1:
        _coordinates = _coordinates[0]
        _type = 'LineString'
    return {"type":_type,
            "coordinates":_coordinates}

def _from_rings(rings):
    """Private parser for converting ESRI JSON rings to (Multi)Polygon.

    @param rings: ring coordinate sequence.
    @type rings: C{list}
    @return: corresponding (Multi)Polygon object in GeoJSON.
    @rtype: C{dict}
    """
    _coordinates = []
    parts = 0
    for ring in rings:
        if is_clockwise(ring):
            parts += 1
            _coordinates.append([ring])
        else:
            _coordinates[-1].append(ring)
    return {"type": 'MultiPolygon' \
                if parts > 1 else 'Polygon',
            "coordinates":_coordinates \
                if parts > 1 else _coordinates[0]
            }

def _from_points(esriGeom):
    """Private parser for converting ESRI JSON point(s) to (Multi)Point.

    @param esriGeom: esri JSON geometry object.
    @type esriGeom: C{dict}
    @return: corresponding (Multi)Point object in GeoJSON.
    @rtype: C{dict}
    """
    parts = 1
    if esriGeom.has_key('points'):
        parts += 1
        _coordinates = esriGeom['points']
    elif esriGeom.has_key('x'):
        _coordinates = [esriGeom['x'], esriGeom['y']]
    else:
        _coordinates = []
    return {"type":"MultiPoint" \
                if parts > 1 else "Point",
            "coordinates" : _coordinates}

def _from_esri_wkid(spatialReference, _for='proj4'):
    """Private function for creating a crs object for GeoJSON.

    Creates a linked crs-object pointing to
    U{spatialreference.org<http://spatialreference.org/>}
    
    @param spatialReference: esri JSON spatialReference object.
    @type spatialReference: C{dict}
    @param _for: argument that can be used to dictate the url request output.
        e.g. proj4 to get the corresponding proj4 string, esriwkt to get the
        esri wkt (for saving to a shapefile). Valid values are C{html},
        C{prettywkt}, C{proj4}, C{json}, C{gml}, C{esriwkt}, C{prj},
        C{usgs}, C{mapfile}, C{mapserverpython}, C{mapnik},
        C{mapnikpython}, C{geoserver}, C{postgis}, C{proj4js}.
        U{GeoJSON spec<http://geojson.org/geojson-spec.html#coordinate-reference-system-objects>}
        itself states:
        "Suggested values are: "proj4", "ogcwkt", "esriwkt", but others can
        be used".
    @return: linked crs object.
    @rtype: C{dict}
    """
    sr = {"f":_for,
          "w":spatialReference['wkid']}
    return {
        "type":"link",
        "properties": {
            "href":"http://spatialreference.org/ref/epsg/%(w)s/%(f)s/" % sr,
            "type":"link"}
        }
