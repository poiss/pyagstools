# -*- coding: utf-8 -*-
"""
Test data for pyags-tools tests.
"""

geojson_validate_url = 'http://geojsonlint.com/validate'

testring = [[0, 0], [0, 10], [10, 10], [10, 0], [0, 0]]

esri_none = {
    "attributes": {
        "objectid": 1,
        "label": "a feature without geometry"
        },
    }

esri_pt = {
    "attributes": {
        "objectid": 1,
        "label": "a simple point"
        },
    "geometry": {
        "x": 26.12345,
        "y": 64.12345
        }
    }

esri_mpt = {
    "attributes": {
        "objectid": 1,
        "label": "a multipoint of two"
        },
    "geometry": {
        "points": [
            [26.54321, 65.123456],
            [27.12345, 64.654321]
            ]
        }
    }

esri_ls = {
    "attributes": {
        "objectid": 1,
        "label": "a simple linestring",
        },
    "geometry": {
        "paths": [
            [
                [26.12345, 65.04547],
                [26.31184, 65.13313],
                [26.3997, 65.65068]
                ]
            ]
        }
    }


esri_mls = {
    "attributes": {
        "objectid": 2,
        "label": "a multipart linestring of two",
        },
    "geometry": {
        "paths": [
            [
                [27.223975, 53.255901],
                [26.12345, 54.123567],
                [22.855678, 56.264731],
                [26.34245, 59.64534],
                [27.23451, 59.12343]
                ],
            [
                [22.6587684, 56.1325456],
                [21.4656, 61.13534521],
                [20.345353, 52.3453453],
                [25.12324534, 51.4324234]
                ]
            ]
        }
    }

esri_p = {
    "attributes": {
        "objectid": 1,
        "label": "a simple polygon",
        },
    "geometry": {
        "rings": [
            [
                [29.123123, 56.33456451],
                [31.2342342, 59.9456353],
                [33.234234, 58.645634],
                [26.2342437, 53.3453455],
                [21.123997, 58.13242342],
                [29.123123,56.33456451]
                ]
            ]
        }
    }

esri_p_holes = {
    "attributes": {
        "objectid": 2,
        "label": "a polygon with two holes",
        },
    "geometry": {
        "rings": [
            [
                [22.123412, 53.000065],
                [22.121234, 57.1235343],
                [25.867546, 57.2345324],
                [25.23464456, 53.11577],
                [22.123412, 53.000065]
                ],
            [
                [23.123412, 54.000065],
                [24.23464456, 54.11577],
                [24.867546, 54.2345324],
                [23.121234, 54.1235343],                
                [23.123412, 54.000065]
                ],
            [
                [24.123412, 55.000065],
                [24.23464456, 55.11577],
                [24.867546, 56.2345324],
                [24.121234, 56.1235343],
                [24.123412, 55.000065]
                ]
            ]
        }
    }

esri_mp_holes = {
    "attributes": {
        "objectid": 3,
        "label": "a 3-part multipolygon with two holes",
        },
    "geometry": {
        "rings": [
            [
                [22.123412, 58.000065],
                [22.121234, 59.1235343],
                [25.867546, 60.2345324],
                [25.23464456, 59.11577],
                [22.123412, 58.000065]
                ],
            [
                [30.123412, 58.000065],
                [30.121234, 59.1235343],
                [32.867546, 60.2345324],
                [32.23464456, 59.11577],
                [30.123412, 58.000065]
                ],
            [
                [26.123412, 53.000065],
                [26.121234, 57.1235343],
                [29.867546, 57.2345324],
                [29.23464456, 53.11577],
                [26.123412, 53.000065]
                ],
            [
                [27.123412, 54.000065],
                [28.23464456, 54.11577],
                [28.867546, 54.2345324],
                [27.121234, 54.1235343],
                [27.123412, 54.000065]
                ],
            [
                [28.123412, 55.000065],
                [28.23464456, 55.11577],
                [28.867546, 56.2345324],
                [28.121234, 56.1235343],
                [28.123412, 55.000065]
                ]
            ]
        }
    }

esri_featureset_p = {
    "displayFieldName" : "label",
    "fieldAliases" : {
        "objectid":"oid",
        "label":"label"},
    "geometryType" : "esriGeometryPolygon",
    "spatialReference" : {
        "wkid" : 4326},
    "fields" : [
        {
            "name" : "objectid",
            "type" : "esriFieldTypeOID",
            "alias" : "oid"},
        {
            "name" : "label",
            "type" : "esriFieldTypeString",
            "alias" : "label",
            "length" : 255}
        ],
    "features" : [
        esri_mp_holes, esri_p, esri_p_holes ]
    }

esri_featureset_ls = {
    "displayFieldName" : "label",
    "fieldAliases" : {
        "objectid":"oid",
        "label":"label"},
    "geometryType" : "esriGeometryPolyline",
    "spatialReference" : {
        "wkid" : 4326},
    "fields" : [
        {
            "name" : "objectid",
            "type" : "esriFieldTypeOID",
            "alias" : "oid"},
        {
            "name" : "label",
            "type" : "esriFieldTypeString",
            "alias" : "label",
            "length" : 255}
        ],
    "features" : [
        esri_ls, esri_mls ]
    }

esri_featureset_pt = {
    "displayFieldName" : "label",
    "fieldAliases" : {
        "objectid":"oid",
        "label":"label"},
    "geometryType" : "esriGeometryPoint", 
    "spatialReference" : {
        "wkid" : 4326},
    "fields" : [
        {
            "name" : "objectid",
            "type" : "esriFieldTypeOID",
            "alias" : "oid"},
        {
            "name" : "label",
            "type" : "esriFieldTypeString",
            "alias" : "label",
            "length" : 255}
        ],
    "features" : [
        esri_pt ]
    }

esri_featureset_pts = {
    "displayFieldName" : "label",
    "fieldAliases" : {
        "objectid":"oid",
        "label":"label"},
    "geometryType" : "esriGeometryMultipoint", 
    "spatialReference" : {
        "wkid" : 4326},
    "fields" : [
        {
            "name" : "objectid",
            "type" : "esriFieldTypeOID",
            "alias" : "oid"},
        {
            "name" : "label",
            "type" : "esriFieldTypeString",
            "alias" : "label",
            "length" : 255}
        ],
    "features" : [
        esri_mpt ]
    }
